#include "smartalloc.h"

smartalloc *smartalloc_create() {
    smartalloc *sa = malloc(sizeof(smartalloc));
    if (!sa) return NULL;
    sa->head = NULL;
    return sa;
}

smartalloc_node *smartalloc_new_node(void *ptr) {
    if (!ptr) return NULL;
    smartalloc_node *new_node = malloc(sizeof(smartalloc_node));
    if (!new_node) return NULL;
    new_node->ptr = ptr;
    new_node->next = NULL;
    return new_node;
}

void smartalloc_add(smartalloc *sa, void *ptr) {
    if (!sa || !ptr) return;
    smartalloc_node *new_node = smartalloc_new_node(ptr);
    if (!new_node) return;
    new_node->next = sa->head;
    sa->head = new_node;
}

void smartalloc_free(smartalloc *sa) {
    if (!sa) return;
    smartalloc_node *node = sa->head;
    while (node) {
        free(node->ptr);
        node->ptr = NULL;
        smartalloc_node *next = node->next;
        node->next = NULL;
        free(node);
        node = NULL;
        node = next;
    }
    free(sa);
    sa = NULL;
}

void *smart_malloc(smartalloc *sa, size_t size) {
    if (!sa || size == 0) return NULL;
    void *malloc_ptr = malloc(size);
    if (!malloc_ptr) return NULL;
    smartalloc_add(sa, malloc_ptr);
    return malloc_ptr;
}

void *smart_calloc(smartalloc *sa, size_t num, size_t size) {
    if (!sa || num == 0 || size == 0) return NULL;
    void *calloc_ptr = calloc(num, size);
    if (!calloc_ptr) return NULL;
    smartalloc_add(sa, calloc_ptr);
    return calloc_ptr;
}
