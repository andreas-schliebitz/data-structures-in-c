#include <stdio.h>
#include <stdlib.h>

#include "smartalloc.h"

int main(void) {
    smartalloc *sa = smartalloc_create();

    for (size_t i = 0; i < 1000; ++i) {
        smart_malloc(sa, 1);
        smart_calloc(sa, i, i);
    }

    smartalloc_free(sa);

    return EXIT_SUCCESS;
}