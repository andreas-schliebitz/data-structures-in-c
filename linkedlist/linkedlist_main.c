#include <assert.h>

#include "linkedlist.h"

int main(void) {
    linkedlist *list = linkedlist_create();

    size_t n = 10;
    for (size_t i = 0; i < n; ++i) {
        linkedlist_emplace_front(list, &i, sizeof(i));
    }

    assert(!linkedlist_empty(list));
    assert(n == linkedlist_size(list));

    for (size_t i = 0; i < n; ++i) {
        linkedlist_emplace_back(list, &i, sizeof(i));
    }

    n *= 2;
    assert(n == linkedlist_size(list));

    linkedlist_reverse(list);

    for (size_t i = 0; i < n; ++i) {
        size_t *front = linkedlist_front(list);
        size_t *back = linkedlist_back(list);
        assert(*front == *back);
    }

    linkedlist_remove(list, n / 2);

    size_t *front_data = linkedlist_pop_front(list);
    assert(*front_data == 9);
    linkedlist_free_data(front_data);

    size_t *back_data = linkedlist_pop_back(list);
    assert(*back_data == 9);
    linkedlist_free_data(back_data);

    n -= 3;
    assert(n == linkedlist_size(list));

    size_t zero = 0;
    linkedlist_insert_after(list, n / 2, &zero, sizeof(zero));

    size_t nine = 9;
    linkedlist_insert_before(list, 0, &nine, sizeof(nine));
    linkedlist_insert_after(list, n + 1, &nine, sizeof(nine));

    n += 3;
    for (size_t i = 0; i < n; ++i) {
        size_t *data_i = linkedlist_get(list, i);
        printf("%ld", *data_i);
    }
    printf("\n");

    linkedlist_clear(list);

    n = 0;
    assert(n == linkedlist_size(list));
    assert(linkedlist_empty(list));

    linkedlist_free(list);

    return EXIT_SUCCESS;
}
