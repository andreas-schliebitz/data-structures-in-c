/*
Probably not the worst implementation of (doubly-)linked list you have ever
seen.

Author: Andreas Schliebitz
Date: 2019-07-14
Source: https://en.wikipedia.org/wiki/Doubly_linked_list
Compile with: $ gcc -O3 -Wall -Werror -Wextra -Wpedantic hashmap_main.c
hashmap.c -o hashmap_main OR use the Makefile: $ make
 */

#include "linkedlist.h"

linkedlist *linkedlist_create() {
    linkedlist *list = malloc(sizeof(linkedlist));
    if (!list) return NULL;
    list->first_node = NULL;
    list->last_node = NULL;
    return list;
}

linkedlist_node *linkedlist_new_node(const void *value, size_t value_size) {
    if (!value || value_size == 0) return NULL;
    void *entry = malloc(value_size);
    if (!entry) return NULL;
    memcpy(entry, value, value_size);
    linkedlist_node *new_node = malloc(sizeof(linkedlist_node));
    if (!new_node) return NULL;
    new_node->data = entry;
    new_node->prev = NULL;
    new_node->next = NULL;
    return new_node;
}

bool linkedlist_insert_after_node(linkedlist *list, linkedlist_node *after_node,
                                  const void *value, size_t value_size) {
    if (!list || !after_node || !value || value_size == 0) return false;
    linkedlist_node *new_node = linkedlist_new_node(value, value_size);
    if (!new_node) return false;
    new_node->prev = after_node;
    if (!after_node->next) {
        new_node->next = NULL;
        list->last_node = new_node;
    } else {
        new_node->next = after_node->next;
        after_node->next->prev = new_node;
    }
    after_node->next = new_node;
    return true;
}

bool linkedlist_insert_after(linkedlist *list, size_t index, const void *value,
                             size_t value_size) {
    if (!value || value_size == 0) return false;
    linkedlist_node *after_node = linkedlist_get_node(list, index);
    if (!after_node) return false;
    return linkedlist_insert_after_node(list, after_node, value, value_size);
}

bool linkedlist_insert_before_node(linkedlist *list,
                                   linkedlist_node *before_node,
                                   const void *value, size_t value_size) {
    if (!list || !before_node || !value || value_size == 0) return false;
    linkedlist_node *new_node = linkedlist_new_node(value, value_size);
    if (!new_node) return false;
    new_node->next = before_node;
    if (!before_node->prev) {
        new_node->prev = NULL;
        list->first_node = new_node;
    } else {
        new_node->prev = before_node->prev;
        before_node->prev->next = new_node;
    }
    before_node->prev = new_node;
    return true;
}

bool linkedlist_insert_before(linkedlist *list, size_t index, const void *value,
                              size_t value_size) {
    if (!list || !value || value_size == 0) return false;
    linkedlist_node *before_node = linkedlist_get_node(list, index);
    if (!before_node) return false;
    return linkedlist_insert_before_node(list, before_node, value, value_size);
}

bool linkedlist_emplace_front(linkedlist *list, const void *value,
                              size_t value_size) {
    if (!list || !value || value_size == 0) return false;
    if (!list->first_node) {
        linkedlist_node *new_node = linkedlist_new_node(value, value_size);
        list->first_node = new_node;
        list->last_node = new_node;
        return true;
    }
    return linkedlist_insert_before_node(list, list->first_node, value,
                                         value_size);
}

bool linkedlist_emplace_back(linkedlist *list, const void *value,
                             size_t value_size) {
    if (!list || !value || value_size == 0) return false;
    if (!list->last_node)
        return linkedlist_emplace_front(list, value, value_size);
    return linkedlist_insert_after_node(list, list->last_node, value,
                                        value_size);
}

linkedlist_node *linkedlist_front_node(linkedlist *list) {
    if (!list) return NULL;
    return list->first_node;
}

linkedlist_node *linkedlist_back_node(linkedlist *list) {
    if (!list) return NULL;
    return list->last_node;
}

void *linkedlist_front(linkedlist *list) {
    if (!list) return NULL;
    return linkedlist_front_node(list)->data;
}

void *linkedlist_back(linkedlist *list) {
    if (!list) return NULL;
    return linkedlist_back_node(list)->data;
}

void *linkedlist_pop_back(linkedlist *list) {
    if (!list) return NULL;
    linkedlist_node *back = linkedlist_back_node(list);
    return linkedlist_remove_node(list, back);
}

void *linkedlist_pop_front(linkedlist *list) {
    if (!list) return NULL;
    linkedlist_node *front = linkedlist_front_node(list);
    return linkedlist_remove_node(list, front);
}

void *linkedlist_remove_node(linkedlist *list, linkedlist_node *remove_node) {
    if (!list || !remove_node) NULL;
    if (!remove_node->prev) {
        list->first_node = remove_node->next;
    } else {
        remove_node->prev->next = remove_node->next;
    }
    if (!remove_node->next) {
        list->last_node = remove_node->prev;
    } else {
        remove_node->next->prev = remove_node->prev;
    }
    void *data = remove_node->data;
    linkedlist_free_node(remove_node, false);
    return data;
}

bool linkedlist_remove(linkedlist *list, size_t index) {
    if (!list) return false;
    linkedlist_node *remove_node = linkedlist_get_node(list, index);
    if (!remove_node) return false;
    linkedlist_node *node = linkedlist_remove_node(list, remove_node);
    if (!node) return false;
    linkedlist_free_node(node, true);
    return true;
}

bool linkedlist_clear(linkedlist *list) {
    if (!list) return false;
    linkedlist_node *node = list->first_node;
    while (node) {
        linkedlist_node *next = node->next;
        linkedlist_free_node(node, true);
        node = next;
    }
    list->first_node = NULL;
    list->last_node = NULL;
    return true;
}

linkedlist_node *linkedlist_get_node(linkedlist *list, size_t index) {
    if (!list) return NULL;
    linkedlist_node *node = list->first_node;
    size_t i = 0;
    while (node) {
        if (index == i) return node;
        i++;
        node = node->next;
    }
    return NULL;
}

void *linkedlist_get(linkedlist *list, size_t index) {
    if (!list) return NULL;
    linkedlist_node *node = list->first_node;
    size_t i = 0;
    while (node) {
        if (index == i) return node->data;
        i++;
        node = node->next;
    }
    return NULL;
}

size_t linkedlist_size(const linkedlist *list) {
    if (!list) return 0;
    size_t size = 0;
    linkedlist_node *node = list->first_node;
    while (node) {
        size++;
        node = node->next;
    }
    return size;
}

bool linkedlist_empty(const linkedlist *list) {
    if (!list) return true;
    return linkedlist_size(list) == 0;
}

bool linkedlist_reverse(linkedlist *list) {
    if (!list) return false;
    linkedlist_node *cur = list->first_node;
    linkedlist_node *tmp = NULL;
    while (cur) {
        tmp = cur->next;
        cur->next = cur->prev;
        cur->prev = tmp;
        cur = tmp;
    }
    tmp = list->first_node;
    list->first_node = list->last_node;
    list->last_node = tmp;
    return true;
}

void linkedlist_free_data(void *data) {
    free(data);
    data = NULL;
}

bool linkedlist_free_node(linkedlist_node *node, bool free_data) {
    if (!node) return false;
    if (free_data) linkedlist_free_data(node->data);
    node->prev = NULL;
    node->next = NULL;
    free(node);
    node = NULL;
    return true;
}

void linkedlist_free(linkedlist *list) {
    if (!list) return;
    linkedlist_clear(list);
    free(list);
    list = NULL;
}
