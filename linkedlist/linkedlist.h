#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#ifdef __cplusplus
}
#endif

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct linkedlist_node {
    void *data;
    struct linkedlist_node *prev;
    struct linkedlist_node *next;
} linkedlist_node;

typedef struct linkedlist {
    struct linkedlist_node *first_node;
    struct linkedlist_node *last_node;
} linkedlist;

linkedlist *linkedlist_create();
bool linkedlist_insert_after(linkedlist *list, size_t index, const void *value,
                             size_t value_size);
bool linkedlist_insert_after_node(linkedlist *list, linkedlist_node *after_node,
                                  const void *value, size_t value_size);
bool linkedlist_insert_before(linkedlist *list, size_t index, const void *value,
                              size_t value_size);
bool linkedlist_insert_before_node(linkedlist *list,
                                   linkedlist_node *before_node,
                                   const void *value, size_t value_size);
bool linkedlist_emplace_front(linkedlist *list, const void *value,
                              size_t value_size);
bool linkedlist_emplace_back(linkedlist *list, const void *value,
                             size_t value_size);
void *linkedlist_pop_back(linkedlist *list);
void *linkedlist_pop_front(linkedlist *list);
void *linkedlist_remove_node(linkedlist *list, linkedlist_node *remove_node);
bool linkedlist_remove(linkedlist *list, size_t index);
bool linkedlist_clear(linkedlist *list);
linkedlist_node *linkedlist_get_node(linkedlist *list, size_t index);
void *linkedlist_get(linkedlist *list, size_t index);
size_t linkedlist_size(const linkedlist *list);
bool linkedlist_empty(const linkedlist *list);
void *linkedlist_front(linkedlist *list);
void *linkedlist_back(linkedlist *list);
linkedlist_node *linkedlist_front_node(linkedlist *list);
linkedlist_node *linkedlist_back_node(linkedlist *list);
bool linkedlist_reverse(linkedlist *list);
linkedlist_node *linkedlist_new_node(const void *value, size_t value_size);
void linkedlist_free_data(void *data);
bool linkedlist_free_node(linkedlist_node *node, bool free_data);
void linkedlist_free(linkedlist *list);

#ifdef __cplusplus
}
#endif

#endif /* LINKEDLIST_H  */
