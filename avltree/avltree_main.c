#include <assert.h>
#include <stdlib.h>
#include <time.h>

#include "avltree.h"

char avl_compare(const void *a, const void *b);
void avltree_inorder(const avltree_node *root);
void avltree_print_node(const avltree_node *avl_node);

char avl_compare(const void *a, const void *b) {
    const size_t *a_typed = a;
    const size_t *b_typed = b;
    if (*a_typed == *b_typed) return 0;
    if (*a_typed < *b_typed) return -1;
    if (*a_typed > *b_typed) return 1;
    return 0;
}

void avltree_inorder(const avltree_node *avl_root) {
    if (avl_root) {
        avltree_inorder(avl_root->left);
        avltree_print_node(avl_root);
        avltree_inorder(avl_root->right);
    }
}

void avltree_print_node(const avltree_node *avl_node) {
    if (!avl_node) {
        printf("AVL node is NULL!\n");
    } else {
        printf("Key value: %ld; Key: %p; Node: %p; Left: %p; Right: %p\n",
               *(size_t *)avl_node->key, (void *)avl_node->key,
               (void *)avl_node, (void *)avl_node->left,
               (void *)avl_node->right);
    }
}

int main(void) {
    srand(time(NULL));

    smartalloc *sa = smartalloc_create();
    avltree *avl_tree = avltree_create(sa, avl_compare);

    size_t n = 1000;
    for (size_t i = 0; i < n; ++i) {
        size_t r = rand() % n;
        avltree_insert(avl_tree, &r, sizeof(r));
        assert(avltree_verify(avl_tree));
    }

    printf("AVL tree after insertion:\n");
    avltree_inorder(avl_tree->root);

    for (size_t i = 0; i < n; ++i) {
        avltree_remove(avl_tree, &i, sizeof(i));
        assert(avltree_verify(avl_tree));
    }

    printf("AVL tree after removal:\n");
    avltree_inorder(avl_tree->root);

    avltree_free(avl_tree);

    return EXIT_SUCCESS;
}
