/*
Probably not the worst implementation of an AVL-tree you have ever
seen.

Author: Andreas Schliebitz
Date: 2019-07-23
Compile with: $ gcc -O3 -Wall -Werror -Wextra -Wpedantic avltree_main.c
avltree.c -o avltree_main OR use the Makefile: $ make
 */

#include "avltree.h"

avltree *avltree_create(smartalloc *sa, const avltree_comparator compare) {
    avltree *avl_tree = malloc(sizeof(avltree));
    if (!avl_tree) return NULL;
    avl_tree->root = NULL;
    avl_tree->sa = sa;
    avl_tree->compare = compare;
    return avl_tree;
}

avltree_node *avltree_new_node(const avltree *avl_tree, const void *key,
                               size_t key_size) {
    if (!key || key_size == 0) return NULL;
    void *key_cpy = avltree_copy_key(avl_tree, key, key_size);
    if (!key_cpy) return NULL;
    avltree_node *avl_node = smart_malloc(avl_tree->sa, sizeof(avltree_node));
    if (!avl_node) return NULL;
    avl_node->key = key_cpy;
    avl_node->height = 1;
    avl_node->left = NULL;
    avl_node->right = NULL;
    return avl_node;
}

void *avltree_copy_key(const avltree *avl_tree, const void *key,
                       size_t key_size) {
    if (!key || key_size == 0) return NULL;
    void *key_cpy = smart_malloc(avl_tree->sa, key_size);
    if (!key_cpy) return NULL;
    return memcpy(key_cpy, key, key_size);
}

size_t avltree_height(const avltree_node *avl_node) {
    if (!avl_node) return 0;
    return avl_node->height;
}

size_t avltree_max(size_t a, size_t b) { return a > b ? a : b; }

size_t avltree_min(size_t a, size_t b) { return a < b ? a : b; }

avltree_node *avltree_rotate_right(avltree_node *avl_node) {
    if (!avl_node) return NULL;
    avltree_node *cur_left = avl_node->left;

    if (!cur_left) return NULL;
    avltree_node *cur_left_right = cur_left->right;

    cur_left->right = avl_node;
    avl_node->left = cur_left_right;

    avl_node->height = avltree_max(avltree_height(avl_node->left),
                                   avltree_height(avl_node->right)) +
                       1;
    cur_left->height = avltree_max(avltree_height(cur_left->left),
                                   avltree_height(cur_left->right)) +
                       1;

    return cur_left;
}

avltree_node *avltree_rotate_left(avltree_node *avl_node) {
    if (!avl_node) return NULL;
    avltree_node *cur_right = avl_node->right;

    if (!cur_right) return NULL;
    avltree_node *cur_right_left = cur_right->left;

    cur_right->left = avl_node;
    avl_node->right = cur_right_left;

    avl_node->height = 1 + avltree_max(avltree_height(avl_node->left),
                                       avltree_height(avl_node->right));
    cur_right->height = 1 + avltree_max(avltree_height(cur_right->left),
                                        avltree_height(cur_right->right));

    return cur_right;
}

int64_t avltree_get_balance(const avltree_node *avl_node) {
    if (!avl_node) return 0;
    return avltree_height(avl_node->left) - avltree_height(avl_node->right);
}

void avltree_insert(avltree *avl_tree, const void *key, size_t key_size) {
    if (!avl_tree || !key || key_size == 0) return;
    avl_tree->root =
        avltree_insert_node(avl_tree, avl_tree->root, key, key_size);
}

avltree_node *avltree_insert_node(const avltree *avl_tree,
                                  avltree_node *avl_node, const void *key,
                                  size_t key_size) {
    if (!avl_node) {
        if (!avl_tree || !key || key_size == 0) return NULL;
        avltree_node *new_node = avltree_new_node(avl_tree, key, key_size);
        if (!new_node) return NULL;
        return new_node;
    }

    if (avl_tree->compare(key, avl_node->key) < 0) {
        avl_node->left =
            avltree_insert_node(avl_tree, avl_node->left, key, key_size);
    } else if (avl_tree->compare(key, avl_node->key) > 0) {
        avl_node->right =
            avltree_insert_node(avl_tree, avl_node->right, key, key_size);
    } else {
        return avl_node;
    }

    avl_node->height = 1 + avltree_max(avltree_height(avl_node->left),
                                       avltree_height(avl_node->right));
    int64_t balance = avltree_get_balance(avl_node);

    if (balance > 1 && avl_tree->compare(key, avl_node->left->key) < 0) {
        return avltree_rotate_right(avl_node);
    }

    if (balance < -1 && avl_tree->compare(key, avl_node->right->key) > 0) {
        return avltree_rotate_left(avl_node);
    }

    if (balance > 1 && avl_tree->compare(key, avl_node->left->key) > 0) {
        avl_node->left = avltree_rotate_left(avl_node->left);
        return avltree_rotate_right(avl_node);
    }

    if (balance < -1 && avl_tree->compare(key, avl_node->right->key) < 0) {
        avl_node->right = avltree_rotate_right(avl_node->right);
        return avltree_rotate_left(avl_node);
    }

    return avl_node;
}

avltree_node *avltree_min_node(avltree_node *avl_node) {
    if (!avl_node) return NULL;
    avltree_node *cur_node = avl_node;
    while (cur_node->left) {
        cur_node = cur_node->left;
    }
    return cur_node;
}

avltree_node *avltree_find(const avltree *avl_tree,
                           const avltree_node *avl_root, const void *key) {
    if (!avl_tree || !avl_root || !key || !avl_root->key) return NULL;
    if (avl_tree->compare(key, avl_root->key) < 0) {
        return avltree_find(avl_tree, avl_root->left, key);
    }
    return avltree_find(avl_tree, avl_root->right, key);
}

void avltree_remove(avltree *avl_tree, const void *key, size_t key_size) {
    if (!avl_tree || !key) return;
    avl_tree->root =
        avltree_remove_node(avl_tree, avl_tree->root, key, key_size);
}

avltree_node *avltree_remove_node(const avltree *avl_tree,
                                  avltree_node *avl_root, const void *key,
                                  size_t key_size) {
    if (!avl_tree || !avl_root || !key || !avl_root->key) return NULL;
    avl_root = avltree_find(avl_tree, avl_root, key);
    if (!avl_root) return NULL;

    if (!avl_root->left || !avl_root->right) {
        avltree_node *avl_tmp_node =
            avl_root->left ? avl_root->left : avl_root->right;
        if (!avl_tmp_node) {
            avl_tmp_node = avl_root;
            avl_root = NULL;
        } else {
            *avl_root = *avl_tmp_node;
        }
    } else {
        avltree_node *avl_tmp_node = avltree_min_node(avl_root->right);
        avl_root->key = avl_tmp_node->key;
        avl_root->right = avltree_remove_node(avl_tree, avl_root->right,
                                              avl_tmp_node->key, key_size);
    }

    avl_root->height = 1 + avltree_max(avltree_height(avl_root->left),
                                       avltree_height(avl_root->right));

    int64_t balance = avltree_get_balance(avl_root);

    if (balance > 1 && avltree_get_balance(avl_root->left) >= 0) {
        return avltree_rotate_right(avl_root);
    }

    if (balance > 1 && avltree_get_balance(avl_root->left) < 0) {
        avl_root->left = avltree_rotate_left(avl_root->left);
        return avltree_rotate_right(avl_root);
    }

    if (balance < -1 && avltree_get_balance(avl_root->right) <= 0) {
        return avltree_rotate_left(avl_root);
    }

    if (balance < -1 && avltree_get_balance(avl_root->right) > 0) {
        avl_root->right = avltree_rotate_right(avl_root->right);
        return avltree_rotate_left(avl_root);
    }

    return avl_root;
}

bool avltree_verify(const avltree *avl_tree) {
    size_t max_height = 0, min_height = 0;
    return avltree_verify_wrapper(avl_tree->root, &max_height, &min_height);
}

bool avltree_verify_wrapper(const avltree_node *avl_root, size_t *max_height,
                            size_t *min_height) {
    if (!avl_root) {
        *max_height = 0;
        *min_height = 0;
        return true;
    }

    size_t left_max_height = 0, left_min_height = 0, right_max_height = 0,
           right_min_height = 0;

    if (!avltree_verify_wrapper(avl_root->left, &left_max_height,
                                &left_min_height))
        return false;
    if (!avltree_verify_wrapper(avl_root->right, &right_max_height,
                                &right_min_height))
        return false;

    *max_height = avltree_max(left_max_height, right_max_height) + 1;
    *min_height = avltree_min(left_min_height, right_min_height) + 1;

    if (*max_height <= 2 * (*min_height)) return true;

    return false;
}

void avltree_free(avltree *avl_tree) {
    if (!avl_tree) return;
    smartalloc_free(avl_tree->sa);
    avl_tree->sa = NULL;
    free(avl_tree);
    avl_tree = NULL;
}
