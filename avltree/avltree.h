#ifndef AVLTREE_H
#define AVLTREE_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "smartalloc.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct avltree_node {
    void *key;
    size_t height;
    struct avltree_node *left;
    struct avltree_node *right;
} avltree_node;

typedef char (*avltree_comparator)(const void *, const void *);

typedef struct avltree {
    avltree_node *root;
    smartalloc *sa;
    avltree_comparator compare;
} avltree;

avltree *avltree_create(smartalloc *sa, const avltree_comparator compare);
avltree_node *avltree_new_node(const avltree *avl_tree, const void *key,
                               size_t key_size);
void *avltree_copy_key(const avltree *avl_tree, const void *key,
                       size_t key_size);
size_t avltree_height(const avltree_node *avl_node);
size_t avltree_min(size_t a, size_t b);
size_t avltree_max(size_t a, size_t b);
avltree_node *avltree_rotate_right(avltree_node *avl_node);
avltree_node *avltree_rotate_left(avltree_node *avl_node);
int64_t avltree_get_balance(const avltree_node *avl_node);
void avltree_insert(avltree *avl_tree, const void *key, size_t key_size);
avltree_node *avltree_insert_node(const avltree *avl_tree,
                                  avltree_node *avl_node, const void *key,
                                  size_t key_size);
avltree_node *avltree_min_node(avltree_node *avl_node);
avltree_node *avltree_find(const avltree *avl_tree,
                           const avltree_node *avl_root, const void *key);
void avltree_remove(avltree *avl_tree, const void *key, size_t key_size);
bool avltree_verify(const avltree *avl_tree);
bool avltree_verify_wrapper(const avltree_node *avl_root, size_t *max_height,
                            size_t *min_height);
avltree_node *avltree_remove_node(const avltree *avl_tree,
                                  avltree_node *avl_root, const void *key,
                                  size_t key_size);
void avltree_free(avltree *avl_tree);

#ifdef __cplusplus
}
#endif

#endif /* AVLTREE_H */
