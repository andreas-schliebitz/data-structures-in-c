#ifndef SMARTALLOC_H
#define SMARTALLOC_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct smartalloc_node {
    void *ptr;
    struct smartalloc_node *next;
} smartalloc_node;

typedef struct smartalloc {
    smartalloc_node *head;
} smartalloc;

smartalloc *smartalloc_create();
smartalloc_node *smartalloc_new_node(void *ptr);
void smartalloc_add(smartalloc *sa, void *ptr);
void smartalloc_free(smartalloc *sa);

void *smart_malloc(smartalloc *sa, size_t size);
void *smart_calloc(smartalloc *sa, size_t num, size_t size);

#ifdef __cplusplus
}
#endif

#endif /* SMARTALLOC_H */
