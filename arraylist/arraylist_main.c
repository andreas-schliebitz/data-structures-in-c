#include <assert.h>

#include "arraylist.h"

int main(void) {
    arraylist *alist = arraylist_create();
    assert(alist);
    assert(alist->size == 0);
    assert(!alist->data);

    size_t n = 10;
    for (size_t i = 0; i < n; ++i) {
        arraylist_emplace_back(alist, &i, sizeof(i));
        size_t *at_i = arraylist_at(alist, i);
        assert(*at_i == i);

        void *find_i = arraylist_find(alist, &i, sizeof(i));
        assert(find_i);
    }

    size_t size = arraylist_size(alist);
    assert(size == n);

    arraylist_reverse(alist);
    arraylist_reverse(alist);

    size_t *front = arraylist_front(alist);
    assert(*front == n - n);

    int64_t front_io = arraylist_indexof(alist, front, sizeof(*front));
    assert(front_io == 0);

    arraylist_swap(alist, 2, size - 3);
    assert(*(size_t *)arraylist_at(alist, 2) == 7);
    assert(*(size_t *)arraylist_at(alist, size - 3) == 2);

    size_t *back = arraylist_back(alist);
    assert(*back == n - 1);

    bool not_empty = arraylist_empty(alist);
    assert(!not_empty);

    arraylist_pop_back(alist);
    assert(!arraylist_at(alist, n - 1));

    size_t size_a = arraylist_size(alist);
    assert(size_a == n - 1);

    arraylist_erase(alist, 0);
    size_t *at_0 = arraylist_at(alist, 0);
    assert(*at_0 == 1);

    size_t size_b = arraylist_size(alist);
    assert(size_b == n - 2);

    size_t a = 0;
    arraylist_insert(alist, 0, &a, sizeof(a));
    size_t *at_0_a = arraylist_at(alist, 0);
    assert(*at_0_a == 0);

    size_t size_c = arraylist_size(alist);
    assert(size_c == n - 1);

    size_t b = 11;
    arraylist_set(alist, 0, &b, sizeof(b));
    size_t *at_0_b = arraylist_at(alist, 0);
    assert(*at_0_b == b);

    arraylist_clear(alist);
    size_t size_d = arraylist_size(alist);
    assert(size_d == 0);

    arraylist_free(alist);

    return EXIT_SUCCESS;
}
