#ifndef ARRAYLIST_H
#define ARRAYLIST_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct arraylist {
    void **data;
    size_t size;
} arraylist;

arraylist *arraylist_create();
bool arraylist_emplace_back(arraylist *alist, const void *value,
                            size_t value_size);
void *arraylist_at(const arraylist *alist, size_t index);
void *arraylist_find(const arraylist *alist, const void *value,
                     size_t value_size);
int64_t arraylist_indexof(const arraylist *alist, const void *value,
                          size_t value_size);
void *arraylist_front(const arraylist *alist);
void *arraylist_back(const arraylist *alist);
bool arraylist_set(const arraylist *alist, size_t index, const void *value,
                   size_t value_size);
bool arraylist_insert(arraylist *alist, size_t index, const void *value,
                      size_t value_size);
bool arraylist_erase(arraylist *alist, size_t index);
bool arraylist_clear(arraylist *alist);
bool arraylist_swap(const arraylist *alist, size_t index_a, size_t index_b);
bool arraylist_reverse(const arraylist *alist);
bool arraylist_pop_back(arraylist *alist);
void arraylist_free(arraylist *alist);
void arraylist_free_entry(arraylist *alist, size_t index);
void *arraylist_memcpy(const void *value, size_t value_size);
size_t arraylist_size(const arraylist *alist);
bool arraylist_empty(const arraylist *alist);

#ifdef __cplusplus
}
#endif

#endif /* ARRAYLIST_H  */
