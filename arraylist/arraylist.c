/*
Probably not the worst implementation of an array list you have ever seen.

Author: Andreas Schliebitz
Date: 2019-07-014
Compile with: $ gcc -O3 -Wall -Werror -Wextra -Wpedantic arraylist_main.c
arraylist.c -o arraylist_main OR use the Makefile: $ make
*/

#include "arraylist.h"

arraylist *arraylist_create() {
    arraylist *alist = malloc(sizeof(arraylist));
    if (!alist) return NULL;
    alist->size = 0;
    alist->data = NULL;
    return alist;
}

bool arraylist_emplace_back(arraylist *alist, const void *value,
                            size_t value_size) {
    if (!alist || !value || value_size == 0) return false;
    void **alist_data =
        realloc(alist->data, (alist->size + 1) * sizeof(void *));
    if (!alist_data) return false;
    alist->data = alist_data;
    void *entry = arraylist_memcpy(value, value_size);
    if (!entry) return false;
    alist->data[alist->size] = entry;
    alist->size++;
    return true;
}

bool arraylist_set(const arraylist *alist, size_t index, const void *value,
                   size_t value_size) {
    if (!alist || index >= alist->size || !value || value_size == 0)
        return false;
    free(alist->data[index]);
    void *entry = arraylist_memcpy(value, value_size);
    if (!entry) return false;
    alist->data[index] = entry;
    return true;
}

bool arraylist_insert(arraylist *alist, size_t index, const void *value,
                      size_t value_size) {
    if (!alist || index >= alist->size || !value || value_size == 0)
        return false;
    void *entry = arraylist_memcpy(value, value_size);
    if (!entry) return false;
    void **alist_data =
        realloc(alist->data, (alist->size + 1) * sizeof(void *));
    if (!alist_data) return false;
    alist->data = alist_data;
    alist->size++;
    for (size_t i = alist->size - 1; i > index; --i) {
        alist->data[i] = alist->data[i - 1];
    }
    alist->data[index] = entry;
    return true;
}

void *arraylist_at(const arraylist *alist, size_t index) {
    if (!alist || index >= alist->size) return NULL;
    return alist->data[index];
}

void *arraylist_find(const arraylist *alist, const void *value,
                     size_t value_size) {
    if (!alist || !value || value_size == 0) return NULL;
    for (size_t i = 0; i < alist->size; ++i) {
        if (alist->data[i] && !memcmp(value, alist->data[i], value_size)) {
            return alist->data[i];
        }
    }
    return NULL;
}

int64_t arraylist_indexof(const arraylist *alist, const void *value,
                          size_t value_size) {
    if (!alist || !value || value_size == 0) return -1;
    for (int64_t i = 0; i < (int64_t)alist->size; ++i) {
        if (alist->data[i] && !memcmp(value, alist->data[i], value_size)) {
            return i;
        }
    }
    return -1;
}

bool arraylist_swap(const arraylist *alist, size_t index_a, size_t index_b) {
    if (!alist || index_a >= alist->size || index_b >= alist->size)
        return false;
    void *a = arraylist_at(alist, index_a);
    void *b = arraylist_at(alist, index_b);
    alist->data[index_a] = b;
    alist->data[index_b] = a;
    return true;
}

bool arraylist_reverse(const arraylist *alist) {
    if (!alist) return false;
    size_t front = 0;
    size_t back = arraylist_size(alist) - 1;
    void *tmp = NULL;
    while (front < back) {
        tmp = alist->data[front];
        alist->data[front++] = alist->data[back];
        alist->data[back--] = tmp;
    }
    return true;
}

void *arraylist_front(const arraylist *alist) {
    if (!alist) return NULL;
    return arraylist_at(alist, 0);
}

void *arraylist_back(const arraylist *alist) {
    if (!alist || alist->size == 0) return NULL;
    return arraylist_at(alist, alist->size - 1);
}

bool arraylist_pop_back(arraylist *alist) {
    if (!alist || alist->size == 0) return false;
    return arraylist_erase(alist, alist->size - 1);
}

bool arraylist_erase(arraylist *alist, size_t index) {
    if (!alist || index >= alist->size) return false;
    arraylist_free_entry(alist, index);
    for (size_t i = index; i < alist->size - 1; ++i) {
        alist->data[i] = alist->data[i + 1];
    }
    void **alist_data =
        realloc(alist->data, (alist->size - 1) * sizeof(void *));
    if (alist->size > 1 && !alist_data) return false;
    alist->data = alist_data;
    alist->size--;
    return true;
}

bool arraylist_clear(arraylist *alist) {
    if (!alist) return false;
    for (size_t i = 0; i < alist->size; ++i) {
        arraylist_free_entry(alist, i);
    }
    alist->size = 0;
    free(alist->data);
    alist->data = NULL;
    return true;
}

size_t arraylist_size(const arraylist *alist) {
    if (!alist) return 0;
    return alist->size;
}

bool arraylist_empty(const arraylist *alist) {
    if (!alist) return true;
    return alist->size == 0;
}

void *arraylist_memcpy(const void *value, size_t value_size) {
    if (!value || value_size == 0) return NULL;
    void *entry = malloc(value_size);
    if (!entry) return NULL;
    return memcpy(entry, value, value_size);
}

void arraylist_free_entry(arraylist *alist, size_t index) {
    if (!alist) return;
    free(alist->data[index]);
    alist->data[index] = NULL;
}

void arraylist_free(arraylist *alist) {
    if (!alist) return;
    arraylist_clear(alist);
    alist->size = 0;
    free(alist);
    alist = NULL;
}
