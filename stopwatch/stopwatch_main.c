#include <unistd.h>
#include "stopwatch.h"

int main(void) {
    stopwatch *sw = stopwatch_create();
    stopwatch_start(sw);

    sleep(2);

    stopwatch_stop(sw);
    stopwatch_free(sw);

    return EXIT_SUCCESS;
}
