#include "stopwatch.h"

stopwatch *stopwatch_create() {
    stopwatch *sw = malloc(sizeof(stopwatch));
    if (!sw) return NULL;
    return sw;
}

void stopwatch_start(stopwatch *sw) {
    sw->cputime_start = clock();
    clock_gettime(CLOCK_REALTIME, &sw->walltime_start);
}

void stopwatch_timespec_diff(stopwatch *sw) {
    if ((sw->walltime_stop.tv_nsec - sw->walltime_start.tv_nsec) < 0) {
        sw->walltime_diff.tv_sec =
            sw->walltime_stop.tv_sec - sw->walltime_start.tv_sec;
        sw->walltime_diff.tv_nsec = STOPWATCH_BILLION +
                                    sw->walltime_stop.tv_nsec -
                                    sw->walltime_start.tv_nsec;
    } else {
        sw->walltime_diff.tv_sec =
            sw->walltime_stop.tv_sec - sw->walltime_start.tv_sec;
        sw->walltime_diff.tv_nsec =
            sw->walltime_stop.tv_nsec - sw->walltime_start.tv_nsec;
    }
}

void stopwatch_stop(stopwatch *sw) {
    if (!sw) return;
    sw->cputime_stop = clock();
    clock_gettime(CLOCK_REALTIME, &sw->walltime_stop);
    stopwatch_timespec_diff(sw);
    sw->cputime_sec =
        ((double)(sw->cputime_stop - sw->cputime_start)) / CLOCKS_PER_SEC;
    sw->cpu_clock_ticks = sw->cputime_stop - sw->cputime_start;
    sw->walltime_sec = sw->walltime_diff.tv_sec + (sw->walltime_diff.tv_nsec / (double)STOPWATCH_BILLION);
    sw->walltime_nsec = sw->walltime_sec * (double)STOPWATCH_BILLION;
    sw->cpu_walltime_delta_sec = fabs(sw->walltime_sec - sw->cputime_sec);
    printf("\nElapsed CPU time in clock ticks: %ld\n", sw->cpu_clock_ticks);
    printf("Elapsed CPU time in seconds: %lf\n", sw->cputime_sec);
    printf("Elapsed wall-time in nanoseconds: %ld\n", sw->walltime_nsec);
    printf("Elapsed wall-time in seconds: %lf\n", sw->walltime_sec);
    printf("CPU and wall-time delta in seconds: %lf\n",
           sw->cpu_walltime_delta_sec);
}

void stopwatch_free(stopwatch *sw) {
    free(sw);
    sw = NULL;
}
