#ifndef STOPWATCH_H
#define STOPWATCH_H

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 199309L
#endif

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define STOPWATCH_BILLION 1000000000

#ifdef __cplusplus
extern "C" {
#endif

typedef struct stopwatch {
    clock_t cputime_start, cputime_stop;
    struct timespec walltime_start, walltime_stop, walltime_diff;
    long cpu_clock_ticks, walltime_nsec;
    double cputime_sec, walltime_sec, cpu_walltime_delta_sec;
} stopwatch;

stopwatch *stopwatch_create();
void stopwatch_start(stopwatch *sw);
void stopwatch_stop(stopwatch *sw);
void stopwatch_timespec_diff(stopwatch *sw);
void stopwatch_free(stopwatch *sw);

#ifdef __cplusplus
}
#endif

#endif /* STOPWATCH_H */
