/*
Probably not the worst implementation of a hashmap you have ever seen.

Only works with '\0'-terminated char* keys!
Neither the keys nor the values can be NULL!

Author: Andreas Schliebitz
Date: 2019-07-14
Compile with: $ gcc -O3 -Wall -Werror -Wextra -Wpedantic hashmap_main.c
hashmap.c -o hashmap_main OR use the Makefile: $ make
*/

#include "hashmap.h"

// https://research.neustar.biz/2011/12/29/choosing-a-good-hash-function-part-2/
uint64_t hashmap_hash(const char *key) {
    size_t key_size = strlen(key) + 1;
    uint64_t hash = 14695981039346656037U;
    for (size_t i = 0; i < key_size; ++i) {
        hash = (hash * 1099511628211) ^ key[i];
    }
    return hash;
}

hashmap *hashmap_create(uint64_t max_size) {
    if (max_size == 0) return NULL;
    hashmap *hmap = malloc(sizeof(hashmap));
    if (!hmap) return NULL;
    hmap->max_size = max_size;
    hmap->size = 0;
    hmap->buckets = calloc(max_size, sizeof(hashmap_bucket *));
    if (!hmap->buckets) return NULL;
    return hmap;
}

hashmap_bucket *hashmap_contains_key(const hashmap *hmap, const char *key) {
    if (!hmap || !key) return false;
    uint64_t hash = hashmap_hash(key);
    uint64_t index = hash % hmap->max_size;
    hashmap_bucket *bucket_head = hashmap_get_bucket_head(hmap, index);
    if (!bucket_head) return NULL;
    hashmap_bucket *current_bucket = bucket_head;
    while (current_bucket) {
        if (hash == current_bucket->hash && !strcmp(key, current_bucket->key))
            return current_bucket;
        current_bucket = current_bucket->next;
    }
    return NULL;
}

hashmap_bucket *hashmap_get_bucket_head(const hashmap *hmap, uint64_t index) {
    if (!hmap || !(hmap->buckets + index)) return NULL;
    return hmap->buckets[index];
}

void hashmap_set_bucket_head(const hashmap *hmap, uint64_t index,
                             hashmap_bucket *bucket_head) {
    if (!hmap || !(hmap->buckets + index)) return;
    hmap->buckets[index] = bucket_head;
}

hashmap_bucket *hashmap_new_bucket(const char *key, uint64_t hash,
                                   const void *value, size_t value_size) {
    if (!key || !value || value_size == 0) return NULL;
    char *key_cpy = malloc(strlen(key) + 1);
    if (!key_cpy) return NULL;
    strcpy(key_cpy, key);
    void *value_cpy = hashmap_copy_value(value, value_size);
    if (!value_cpy) return NULL;
    hashmap_bucket *bucket = malloc(sizeof(hashmap_bucket));
    bucket->key = key_cpy;
    bucket->value = value_cpy;
    bucket->hash = hash;
    bucket->next = NULL;
    return bucket;
}

void *hashmap_copy_value(const void *value, size_t value_size) {
    if (!value || value_size == 0) return NULL;
    void *value_cpy = malloc(value_size);
    if (!value_cpy) return NULL;
    return memcpy(value_cpy, value, value_size);
}

bool hashmap_insert(hashmap *hmap, const char *key, const void *value,
                    size_t value_size) {
    if (!hmap || !key || !value || value_size == 0) return false;
    hashmap_bucket *bucket = hashmap_contains_key(hmap, key);
    if (bucket) {
        free(bucket->value);
        void *value_cpy = hashmap_copy_value(value, value_size);
        if (!value_cpy) return false;
        bucket->value = value_cpy;
    } else {
        uint64_t hash = hashmap_hash(key);
        uint64_t index = hash % hmap->max_size;
        hashmap_bucket *new_bucket =
            hashmap_new_bucket(key, hash, value, value_size);
        if (!new_bucket) return false;
        new_bucket->next = hashmap_get_bucket_head(hmap, index);
        hashmap_set_bucket_head(hmap, index, new_bucket);
        hmap->size++;
    }
    return true;
}

void *hashmap_find(const hashmap *hmap, const void *value, size_t value_size) {
    if (!hmap || !value || value_size == 0) return NULL;
    for (uint64_t i = 0; i < hmap->max_size; ++i) {
        hashmap_bucket *bucket_head = hashmap_get_bucket_head(hmap, i);
        hashmap_bucket *current_bucket = bucket_head;
        while (current_bucket) {
            if (!memcmp(value, current_bucket->value, value_size))
                return current_bucket->value;
            current_bucket = current_bucket->next;
        }
    }
    return NULL;
}

void *hashmap_at(const hashmap *hmap, const char *key) {
    if (!hmap || !key) return NULL;
    hashmap_bucket *bucket = hashmap_contains_key(hmap, key);
    if (bucket) return bucket->value;
    return NULL;
}

bool hashmap_clear(hashmap *hmap) {
    if (!hmap) return false;
    for (uint64_t i = 0; i < hmap->max_size; ++i) {
        hashmap_bucket *bucket_head = hashmap_get_bucket_head(hmap, i);
        hashmap_free_bucketlist(bucket_head);
    }
    hmap->size = 0;
    return true;
}

uint64_t hashmap_max_size(const hashmap *hmap) {
    if (!hmap) return 0;
    return hmap->max_size;
}

uint64_t hashmap_size(const hashmap *hmap) {
    if (!hmap) return 0;
    return hmap->size;
}

bool hashmap_empty(const hashmap *hmap) {
    if (!hmap) return true;
    return hashmap_size(hmap) == 0;
}

bool hashmap_erase(hashmap *hmap, const char *key) {
    if (!hmap || !key) return false;
    uint64_t index = hashmap_hash(key) % hmap->max_size;
    hashmap_bucket *bucket_head = hashmap_get_bucket_head(hmap, index);
    if (bucket_head) {
        bucket_head = hashmap_delete_bucket(bucket_head, key);
        hashmap_set_bucket_head(hmap, index, bucket_head);
        hmap->size--;
    }
    return true;
}

hashmap_bucket *hashmap_delete_bucket(hashmap_bucket *current_bucket,
                                      const char *key) {
    if (!current_bucket) return NULL;
    if (!strcmp(current_bucket->key, key)) {
        hashmap_bucket *tmp_next_bucket = current_bucket->next;
        hashmap_free_bucket(current_bucket);
        free(current_bucket);
        current_bucket = NULL;
        return tmp_next_bucket;
    }
    current_bucket->next = hashmap_delete_bucket(current_bucket->next, key);
    return current_bucket;
}

void hashmap_free_bucket(hashmap_bucket *bucket) {
    if (!bucket) return;
    free(bucket->key);
    bucket->key = NULL;
    free(bucket->value);
    bucket->value = NULL;
    bucket->hash = 0;
    bucket->next = NULL;
}

void hashmap_free_bucketlist(hashmap_bucket *bucket_head) {
    hashmap_bucket *tmp = NULL;
    while (bucket_head) {
        tmp = bucket_head;
        bucket_head = bucket_head->next;
        hashmap_free_bucket(tmp);
    }
}

void hashmap_free_buckets(hashmap *hmap) {
    if (!hmap) return;
    for (uint64_t i = 0; i < hmap->max_size; ++i) {
        free(hashmap_get_bucket_head(hmap, i));
        hashmap_set_bucket_head(hmap, i, NULL);
    }
    free(hmap->buckets);
    hmap->buckets = NULL;
    hmap->max_size = 0;
}

void hashmap_free(hashmap *hmap) {
    if (!hmap) return;
    hashmap_clear(hmap);
    hashmap_free_buckets(hmap);
    free(hmap);
    hmap = NULL;
}
