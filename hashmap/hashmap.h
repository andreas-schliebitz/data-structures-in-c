#ifndef HASHMAP_H
#define HASHMAP_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct hashmap_bucket {
    char *key;
    void *value;
    uint64_t hash;
    struct hashmap_bucket *next;
} hashmap_bucket;

typedef struct hashmap {
    uint64_t max_size;
    uint64_t size;
    hashmap_bucket **buckets;
} hashmap;

uint64_t hashmap_hash(const char *key);
hashmap *hashmap_create(uint64_t max_size);
hashmap_bucket *hashmap_contains_key(const hashmap *hmap, const char *key);
hashmap_bucket *hashmap_new_bucket(const char *key, uint64_t hash,
                                   const void *value, size_t value_size);
hashmap_bucket *hashmap_get_bucket_head(const hashmap *hmap, uint64_t index);
void hashmap_set_bucket_head(const hashmap *hmap, uint64_t index,
                             hashmap_bucket *bucket_head);
void *hashmap_copy_value(const void *value, size_t value_size);
bool hashmap_insert(hashmap *hmap, const char *key, const void *value,
                    size_t value_size);
void *hashmap_find(const hashmap *hmap, const void *value, size_t value_size);
void *hashmap_at(const hashmap *hmap, const char *key);
uint64_t hashmap_max_size(const hashmap *hmap);
uint64_t hashmap_size(const hashmap *hmap);
bool hashmap_empty(const hashmap *hmap);
hashmap_bucket *hashmap_delete_bucket(hashmap_bucket *current_bucket,
                                      const char *key);
bool hashmap_erase(hashmap *hmap, const char *key);
bool hashmap_clear(hashmap *hmap);
void hashmap_free_bucket(hashmap_bucket *bucket);
void hashmap_free_bucketlist(hashmap_bucket *bucket_head);
void hashmap_free_buckets(hashmap *hmap);
void hashmap_free(hashmap *hmap);

#ifdef __cplusplus
}
#endif

#endif /* HASHMAP_H */
