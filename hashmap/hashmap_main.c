#include <assert.h>

#include "hashmap.h"

int main(void) {
    uint64_t max_size = 16;
    hashmap *hmap = hashmap_create(max_size);

    size_t n = 6;
    const char keys[][6] = {"abc", "def", "ghi", "jkl", "mno", "pqr"};

    for (size_t i = 0; i < n; ++i) {
        hashmap_insert(hmap, keys[i], &i, sizeof(i));

        size_t *at_i = hashmap_at(hmap, keys[i]);
        assert(*at_i == i);

        size_t *find_i = hashmap_find(hmap, &i, sizeof(i));
        assert(*find_i == i);
    }

    size_t j = n * 2;
    for (size_t i = 0; i < n; ++i) {
        hashmap_insert(hmap, keys[i], &j, sizeof(j));

        size_t *at_j = hashmap_at(hmap, keys[i]);
        assert(*at_j == j);

        size_t *find_j = hashmap_find(hmap, &j, sizeof(j));
        assert(*find_j == j);
        j++;
    }

    assert(max_size == hashmap_max_size(hmap));
    assert(n == hashmap_size(hmap));
    assert(!hashmap_empty(hmap));

    hashmap_erase(hmap, "ghi");
    assert(n - 1 == hashmap_size(hmap));

    for (uint64_t i = 0; i < hmap->max_size; ++i) {
        hashmap_bucket *head_bucket = hmap->buckets[i];
        hashmap_bucket *current_bucket = head_bucket;
        while (current_bucket) {
            printf("Key: %s, Value: %ld, Hash: %lu, Index: %lu\n",
                   current_bucket->key, *(size_t *)current_bucket->value,
                   current_bucket->hash, current_bucket->hash % hmap->max_size);
            current_bucket = current_bucket->next;
        }
    }

    hashmap_clear(hmap);
    assert(0 == hashmap_size(hmap));
    assert(hashmap_empty(hmap));

    hashmap_free(hmap);

    return EXIT_SUCCESS;
}
